#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
@author: Miroslaw Janieiwcz <miroslaw dot janiewicz at Google's mail>
"""

import sys, os, argparse, base64, StringIO
from PIL import Image, ImageFont, ImageDraw

parser = argparse.ArgumentParser(
                                 description="This is little script that I created just to see if I can and it seems that I do:)\
                                 I may use it one day to generate MOTD texts\
                                 but can't really see any other use for it at the minute... So yeah, pretty pointless.",
                                 epilog="Hope you find it useful, have fun!",
                                 version="0.2",
                                 )

parser.add_argument(dest="text", help="Text that is to be translated")
parser.add_argument(dest="size", type=int, help="Font size to be used")
parser.add_argument("-f", "--font", dest="font", help="Full path to a TTF font to be used")
parser.add_argument("-c", "--char", dest="char", help="A single character that will be used to 'draw' a 'pixel'")
parser.add_argument("-b", "--background", dest="bg_char", help="A single character that will be used to fill the background")

# parse command line options
args = parser.parse_args()

# set font, if no custom font given with -f then fall back to default
if args.font:
    try:
        font = ImageFont.truetype(args.font, args.size)
    except IOError, e:
        print "Can't load the font - check if the path is correct:", e
        sys.exit(1)
else:
    font = ImageFont.load_default()

# grab character that will be used to draw foreground
if args.char:
    char = args.char[0:1]
else:
    char = '#'

# grab character that will be used to fill background
if args.bg_char:
    bg_char = args.bg_char[0:1]
else:
    bg_char = ' '

tx,ty = font.getsize(args.text)
ty=int(ty*2) # returned height is too small (bug?), so lets increase it artificially
tx += 4 # and lets and some margins

# Now as we know what size of image is actually needed we create new image to match it
img = Image.new('1',(tx,ty), 1)
idr = ImageDraw.Draw(img)
idr.text((2,0), args.text, font=font)

for y in range(0,ty):
        line= ''
        empty_lines = 0
        for x in range(0,tx):
                pix = img.getpixel((x,y))
                if pix == 0:
                        line+= char
                else:
                        line+= bg_char

        # if a line contains at least one single foreground character then print the line
        # this is to avoid excessive 'blank' lines
        if char in line:
            print line
